#include <sys/types.h>
#include <iostream>
#include "tetromino.hpp"

#pragma region Tetromino

Tetromino::Tetromino(uint y, uint x){
	Tetromino::y = y;
	Tetromino::x = x;
	Tetromino::uiRot = 0;
	Tetromino::isAlive = true;
}

Tetromino::Tetromino(){
	Tetromino::y = 0;
	Tetromino::x = 0;
	Tetromino::uiRot = 0;
	Tetromino::isAlive = true;
}

// not used
uint Tetromino::GetRot() const{
	return this->uiRot;
}

// resets the last rotation
void Tetromino::ResetRotation(){
	Tetromino::shapeNew = Tetromino::shapeOld;
}

// rotates at least one time clockwise
void Tetromino::RotateCW(uint uiRot){
	uint uiTetSize = this->GetSize();
	this->shapeOld = this->shapeNew;
	
	switch(uiRot % 4){
		case 0:
		break;
		case 1:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++){
				Tetromino::shapeNew[uiRow * uiTetSize + uiCol] = Tetromino::shapeOld[(uiTetSize - uiCol -1) * uiTetSize + uiRow];
			}
		}
		break;
		case 2:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++)
				Tetromino::shapeNew[uiRow * uiTetSize + uiCol] = Tetromino::shapeOld[(uiTetSize - uiRow -1) * uiTetSize + (uiTetSize - uiCol -1)];
		}
		break;
		case 3:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++)
				Tetromino::shapeNew[uiRow * uiTetSize + uiCol] = Tetromino::shapeOld[uiCol * uiTetSize + (uiTetSize - uiRow -1)];
		}
		break;
		default:
		break;
	}
}


// rotates at least one time counter clockwise
void Tetromino::RotateCCW(uint uiRot){
	uint posNew, posOld;
	bool isPosNewPositive, isPosNewInUpperBounds, isPosOldPositive, isPosOldInUpperBounds;
	uint uiTetSize = this->GetSize();
	Tetromino::shapeOld = Tetromino::shapeNew;
	
	switch(uiRot % 4){
		case 0:
		break;
		case 1:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++){
				posNew  = uiRow * uiTetSize + uiCol;
				isPosNewPositive = posNew >= 0;
				isPosNewInUpperBounds = posNew < uiTetSize*uiTetSize;
				assert(isPosNewPositive);
				assert(isPosNewInUpperBounds);
				
				posOld = uiCol * uiTetSize + (uiTetSize - uiRow - 1);
				isPosOldPositive = posOld >= 0;
				isPosOldInUpperBounds = posOld < uiTetSize*uiTetSize;
				assert(isPosOldPositive);
				assert(isPosOldInUpperBounds);
				
				this->shapeNew[posNew] = this->shapeOld[posOld];
			}
		}
		break;
		case 2:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++){
				posNew  = uiRow * uiTetSize + uiCol;
				isPosNewPositive = posNew >= 0;
				isPosNewInUpperBounds = posNew < uiTetSize*uiTetSize;
				assert(isPosNewPositive);
				assert(isPosNewInUpperBounds);
				
				posOld = (uiTetSize - uiRow -1) * uiTetSize + (uiTetSize - uiCol -1);
				isPosOldPositive = posOld >= 0;
				isPosOldInUpperBounds = posOld < uiTetSize*uiTetSize;
				assert(isPosOldPositive);
				assert(isPosOldInUpperBounds);
				
				this->shapeNew[posNew] = this->shapeOld[posOld];
			}
		}
		break;
		case 3:
		for(uint uiRow = 0; uiRow < uiTetSize; uiRow++){
			for(uint uiCol = 0; uiCol < uiTetSize; uiCol++){
				posNew  = uiRow * uiTetSize + uiCol;
				isPosNewPositive = posNew >= 0;
				isPosNewInUpperBounds = posNew < uiTetSize*uiTetSize;
				assert(isPosNewPositive);
				assert(isPosNewInUpperBounds);
				
				posOld = (uiTetSize - uiCol -1) * uiTetSize + uiRow;
				isPosOldPositive = posOld >= 0;
				isPosOldInUpperBounds = posOld < uiTetSize*uiTetSize;
				assert(isPosOldPositive);
				assert(isPosOldInUpperBounds);
				
				this->shapeNew[posNew] = this->shapeOld[posOld];
			}
		}
		break;
		default:
		break;
	}
	
}

/*

uint T_Tetromino::rotated(uint uiX, uint uiY, uint uiRot) const
{
	uint p_i = 0;
	switch (uiRot % 4)
	{
	case 0: // 0 degrees							// 0  1  2
		p_i = uiY * this->getSize() + uiX;		// 3  4  5
		break;											// 6  7  8
														

	case 1: // 90 degrees									// 6  3  0
		p_i = 6 + uiY - (uiX * this->getSize());		// 7  4  1
		break;													// 8  5  2


	case 2: // 180 degrees						
		p_i = 8 - (uiY * this->getSize()) - uiX;		// 8  7  6
		break;													// 5  4  3
																	// 2  1  0

	case 3: // 270 degrees						
		p_i = 2 - uiY + (uiX * this->getSize());		// 2  5  8
		break;													// 1  4  7
	}																// 0  3  6

	return p_i;
}

unit T_Tetromino::rotated(uint x, uint y, uint rot) const
{
	uint p_i = 0;
	switch (rot % this->uiSize)
	{
	case 0: // 0 degrees						// 0  1  2  3
		p_i = y * this->uiSize + x;			// 4  5  6  7
		break;										// 8  9 10 11
														//12 13 14 15

	case 1: // 90 degrees							//12  8  4  0
		p_i = 12 + y - (x * this->uiSize);		//13  9  5  1
		break;											//14 10  6  2
															//15 11  7  3

	case 2: // 180 degrees							//15 14 13 12
		p_i = 15 - (y * this->uiSize) - x;		//11 10  9  8
		break;											// 7  6  5  4
															// 3  2  1  0

	case 3: // 270 degrees							// 3  7 11 15
		p_i = 3 - y + (x * this->uiSize);		// 2  6 10 14
		break;											// 1  5  9 13
	}														// 0  4  8 12

	return p_i;
}
*/
#pragma endregion Tetromino


#pragma region Two_Tetromino
Two_Tetromino::Two_Tetromino(uint x, uint y):Tetromino(x, y){
}

uint Two_Tetromino::GetSize() const{
	assert(this->uiSize == 2);
	return this->uiSize;
}
#pragma endregion Two_Tetromino

#pragma region Three_Tetromino
Three_Tetromino::Three_Tetromino(uint x, uint y):Tetromino(x, y){
}

uint Three_Tetromino::GetSize() const{
	assert(this->uiSize == 3);
	return this->uiSize;
}
#pragma endregion Three_Tetromino

#pragma region Four_Tetromino
Four_Tetromino::Four_Tetromino(uint x, uint y):Tetromino(x, y){
}

uint Four_Tetromino::GetSize() const{
	assert(this->uiSize == 4);
	return this->uiSize;
}
#pragma endregion Four_Tetromino

#pragma region O_Tetromino
O_Tetromino::O_Tetromino(uint x, uint y):Two_Tetromino(x, y){
	O_Tetromino::Init();
}

// the 4-block shape
void O_Tetromino::Init(){
	O_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	O_Tetromino::shapeNew[0] = O_Tetromino::f;
	O_Tetromino::shapeNew[1] = O_Tetromino::f;
	O_Tetromino::shapeNew[2] = O_Tetromino::f;
	O_Tetromino::shapeNew[3] = O_Tetromino::f;
}

uint O_Tetromino::GetX_preview() const{
	return O_Tetromino::x_preview;
}

uint O_Tetromino::GetY_preview() const{
	return O_Tetromino::y_preview;
}

std::vector<eFieldElement> O_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement O_Tetromino::GetFormType() const{
	return O_Tetromino::f;
}

#pragma endregion O_Tetromino

#pragma region T_Tetromino
T_Tetromino::T_Tetromino(uint x, uint y):Three_Tetromino(x, y){
	T_Tetromino::Init();
}

void T_Tetromino::Init(){
	/*
	[0][0][0]
	[2][2][2]
	[0][2][0]
	*/
	T_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	T_Tetromino::shapeNew[3] = T_Tetromino::f;
	T_Tetromino::shapeNew[4] = T_Tetromino::f;
	T_Tetromino::shapeNew[5] = T_Tetromino::f;
	T_Tetromino::shapeNew[7] = T_Tetromino::f;
}

uint T_Tetromino::GetX_preview() const{
	return T_Tetromino::x_preview;
}

uint T_Tetromino::GetY_preview() const{
	return T_Tetromino::y_preview;
}

eFieldElement T_Tetromino::GetFormType() const{
	return T_Tetromino::f;
}

std::vector<eFieldElement> T_Tetromino::GetShape() const{
	return this->shapeNew;
}

#pragma endregion T_Tetromino

#pragma region L_Tetromino
L_Tetromino::L_Tetromino(uint x, uint y):Three_Tetromino(x, y){
	L_Tetromino::Init();
}

void L_Tetromino::Init(){
	/*
	[0][3][0]
	[0][3][0]
	[0][3][3]
	*/
	L_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	L_Tetromino::shapeNew[1] = L_Tetromino::f;
	L_Tetromino::shapeNew[4] = L_Tetromino::f;
	L_Tetromino::shapeNew[7] = L_Tetromino::f;
	L_Tetromino::shapeNew[8] = L_Tetromino::f;
}

uint L_Tetromino::GetX_preview() const{
	return L_Tetromino::x_preview;
}
uint L_Tetromino::GetY_preview() const{
	return L_Tetromino::y_preview;
}

std::vector<eFieldElement> L_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement L_Tetromino::GetFormType() const{
	return L_Tetromino::f;
}

#pragma endregion L_Tetromino

#pragma region L2_Tetromino
L2_Tetromino::L2_Tetromino(uint x, uint y):Three_Tetromino(x, y){
	L2_Tetromino::Init();
}

void L2_Tetromino::Init(){
	L2_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	L2_Tetromino::shapeNew[2] = L2_Tetromino::f;
	L2_Tetromino::shapeNew[5] = L2_Tetromino::f;
	L2_Tetromino::shapeNew[8] = L2_Tetromino::f;
	L2_Tetromino::shapeNew[7] = L2_Tetromino::f;
}

uint L2_Tetromino::GetX_preview() const{
	return L2_Tetromino::x_preview;
}
uint L2_Tetromino::GetY_preview() const{
	return L2_Tetromino::y_preview;
}

std::vector<eFieldElement> L2_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement L2_Tetromino::GetFormType() const{
	return L2_Tetromino::f;
}

#pragma endregion L2_Tetromino

#pragma region Z_Tetromino
Z_Tetromino::Z_Tetromino(uint x, uint y):Three_Tetromino(x, y){
	Z_Tetromino::Init();
}

void Z_Tetromino::Init(){
	Z_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	Z_Tetromino::shapeNew[0] = Z_Tetromino::f;
	Z_Tetromino::shapeNew[1] = Z_Tetromino::f;
	Z_Tetromino::shapeNew[4] = Z_Tetromino::f;
	Z_Tetromino::shapeNew[5] = Z_Tetromino::f;
}

uint Z_Tetromino::GetX_preview() const{
	return Z_Tetromino::x_preview;
}
uint Z_Tetromino::GetY_preview() const{
	return Z_Tetromino::y_preview;
}

std::vector<eFieldElement> Z_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement Z_Tetromino::GetFormType() const{
	return Z_Tetromino::f;
}

#pragma endregion Z_Tetromino

#pragma region S_Tetromino
S_Tetromino::S_Tetromino(uint x, uint y):Three_Tetromino(x, y){
	S_Tetromino::Init();
}

void S_Tetromino::Init(){
	S_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	S_Tetromino::shapeNew[1] = S_Tetromino::f;
	S_Tetromino::shapeNew[2] = S_Tetromino::f;
	S_Tetromino::shapeNew[3] = S_Tetromino::f;
	S_Tetromino::shapeNew[4] = S_Tetromino::f;
}

uint S_Tetromino::GetX_preview() const{
	return S_Tetromino::x_preview;
}
uint S_Tetromino::GetY_preview() const{
	return S_Tetromino::y_preview;
}


std::vector<eFieldElement> S_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement S_Tetromino::GetFormType() const{
	return S_Tetromino::f;
}

#pragma endregion S_Tetromino

#pragma region I_Tetromino
I_Tetromino::I_Tetromino(uint x, uint y):Four_Tetromino(x, y){
	I_Tetromino::Init();
}

void I_Tetromino::Init(){
	I_Tetromino::shapeNew = std::vector<eFieldElement>(this->uiSize*this->uiSize);
	I_Tetromino::shapeNew[1] = I_Tetromino::f;
	I_Tetromino::shapeNew[5] = I_Tetromino::f;
	I_Tetromino::shapeNew[9] = I_Tetromino::f;
	I_Tetromino::shapeNew[13] = I_Tetromino::f;
}

uint I_Tetromino::GetX_preview() const{
	return I_Tetromino::x_preview;
}
uint I_Tetromino::GetY_preview() const{
	return I_Tetromino::y_preview;
}

std::vector<eFieldElement> I_Tetromino::GetShape() const{
	return this->shapeNew;
}

eFieldElement I_Tetromino::GetFormType() const{
	return I_Tetromino::f;
}

#pragma endregion I_Tetromino