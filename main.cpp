#include <stdint.h>
#include <thread>
#include <chrono>
#include <iostream>
#include <fstream>
#include "gamefield.hpp"

void testField(const Gamefield &g);
void testFullField(const Gamefield &g);
void testMoving(Gamefield &g, uint uiTimes=1);
void initnCurses();
void processKeys(Gamefield &g);
bool printGameOver(bool isSuccess);

int main(){
	Gamefield g;
	initnCurses();
	g.InitColors();
	g.InitWins();
	bool isGameOver = false;
	while(!isGameOver){
		// refresh screen and gamefield when counter reaches its maximum
		if(g.uiCounter++ > g.uiMaxCounter){
			g.Update();
			g.Display_board();
			g.Display_nextTetromino();
			g.Display_info();
			g.Display_score();
			// reset the counter
			g.uiCounter = 0;
			
			// for command line execution, not fully implemented:
			//testFullField(g);
		}
		
		processKeys(g);
		// suspends the process for 10 ms
		std::this_thread::sleep_for(std::chrono::milliseconds(10));
		// copies the virtual screen to physical screen
		doupdate();
	}
	wclear(stdscr);
	endwin();
	return 0;
}

// NCURSES initialization
void initnCurses() {
	initscr();             			// initialize curses
	cbreak();              			// disable line buffering, to pass each key press directly to program
	noecho(); 						// prevent echo from key presses to screen
	keypad(stdscr, TRUE);  	// allows arrow keys
	timeout(0);            		// no blocking on getch()
	curs_set(0);           		// set the cursor to invisible
}


// processes the input from command line
void processKeys(Gamefield &g){
	switch (getch()) {
	case KEY_LEFT:
	case 'a':
		g.TryMoveSideways(false);
		g.Display_board();
		break;
	case KEY_RIGHT:
	case 'd':
		g.TryMoveSideways(true);
		g.Display_board();
		break;
	case KEY_UP:
	case 'w':
		g.RotateTetromino();
		g.Display_board();
		break;
	case KEY_DOWN:
	case 's':
		g.MoveTetromino();
		g.Display_board();
		break;
	case ' ': // space bar
		while(g.MoveTetromino());
		g.Display_board();
		break;
	case 'q': // quit game
		endwin();
		if(g.eState == eGameState::eGameOver)
			printGameOver(true);
		else
			printGameOver(false);
		getch();
		exit(0);
		break;
	case 'p': // pause game
		wclear(g.board);
		// deacitvate the update method and display the pause state
		g.eState = eGameState::ePaused;
		g.Display_info();
		wrefresh(g.info);
		// now stop everything, blocking read
		timeout(-1);
		// resume game
		getch();
		if (g.IsGameOver())
			g.eState = eGameState::eGameOver;
		else
			g.eState = eGameState::eRunning;
		g.Display_info();
		timeout(0); // non-blocking read on getch()
		break;
	default:
		break;
	}
}

bool printGameOver(bool isSuccess){
	std::string line;
	std::ifstream gameOverStream;
	
	if(isSuccess)
		gameOverStream.open( "./images/ascii_success.txt" );
	else
		gameOverStream.open( "./images/ascii_fail.txt" );

	
	if( !gameOverStream ) {
		std::cerr << "Datei nicht gefunden" << std::endl;
		return false;
	}
	
	if( !gameOverStream.is_open() ) {
		std::cerr << "Datei konnte nicht geöffnet werden" << std::endl;
		return false;
	}
	/*
	while (gameOverStream >> std::noskipws >> line){
		std::cout << line << std::endl;
	}
	*/
	
	char c;

  while (gameOverStream.get(c))
  {
    if (c == '/') 
    { 
      char last = c; 
      if (gameOverStream.get(c) && c == '/')
      {
        // std::cout << "Read to EOL\n";
        while(gameOverStream.get(c) && c != '\n'); // this comment will be skipped
        // std::cout << "go to next line\n";
        gameOverStream.putback(c);
        continue;
      }
     else { gameOverStream.putback(c); c = last; }
    }
    std::cout << c;
  }
	
	
	gameOverStream.close();
	return true;
}


// plain unformatted field output on the command line just with numbers
// without current Tetromino
void testField(const Gamefield &g){
	std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight> field = g.GetField();
	for(uint uiRow=0; uiRow < Gamefield::uiFieldHeight; uiRow++){
		for(uint uiCol=0; uiCol < Gamefield::uiFieldWidth; uiCol++){
			std::cout << (uint)field[uiRow][uiCol] << " ";
		}
		std::cout << std::endl;
	}
}

// plain unformatted field output on the command line just with numbers
// with current Tetromino
void testFullField(const Gamefield &g){
	std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  field = g.GetFieldWithMovingTetromino();
	for(uint uiRow=0; uiRow < Gamefield::uiFieldHeight; uiRow++){
		for(uint uiCol=0; uiCol < Gamefield::uiFieldWidth; uiCol++){
			std::cout << (signed int)field[uiRow][uiCol] << " ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

// debugging function for Tetromino movement
void testMoving(Gamefield &g, uint uiTimes){
	bool bMoving = false;
	std::pair<uint, uint> posT;
	std::cout << posT.first << " " << posT.second <<  std::endl;
	for(uint uiMoves = 0; uiMoves < uiTimes; uiMoves++){

		bMoving = g.MoveTetromino();
		std::cout << "Moved " << bMoving << std::endl;
		
		posT = g.GetTetrominoPos();
		std::cout << posT.first << " " << posT.second <<  std::endl;
		bMoving = false;
	}
}
