#pragma once
#include <sys/types.h>
#include <iostream>
#include <array>
#include <stdlib.h>

#include "tetromino.hpp"

enum class eGameState: uint { eRunning, ePaused, eGameOver };

class Gamefield
{
	public:
		static const uint uiFieldWidth = 13, uiFieldHeight = 20;	// field width and height
		uint uiCounter = 0, uiMaxCounter = 100;						// counter for gamespeed
		eGameState eState;													// game state, one of: running, paused or game over
		WINDOW *board, *nextTetromino, *score, *info;			// different windows for textual and graphical output
		void Init();																	// initializes the game field, Tetromino and game stats
		void InitWins();															// initializes the window
		void InitColors();														// initializes the colors for the window
		void Update();															// updates the game stats
		bool MoveTetromino(uint uiTimes = 1);							// tries to move the Tetromino downwards
		bool TryMoveSideways(bool right) const;						// tries to move the Tetromino sidewards
		bool IsColliding() const;												// collsion check with the environment
		bool RotateTetromino(uint uiTimes = 1) const;				// tries to rotate the Tetromino
		bool IsGameOver() const;											// shows if game is over
		eFieldElement GetFormType();										// gets the form type of the Tetromino
		std::pair<uint, uint> GetTetrominoPos() const;				// gets the y and x postion of the Tetromino
		std::array<std::array<eFieldElement, uiFieldWidth>, uiFieldHeight> GetField() const;									// gets the whole filed without Tetromino
		std::array<std::array<eFieldElement, uiFieldWidth>, uiFieldHeight> GetFieldWithMovingTetromino() const;		// gets the whole filed with Tetromino
		void Display_board() const;											// draws the board
		void Display_nextTetromino();										// draws the next Tetromino
		void Display_info();														// draws the game information / status window
		void Display_score() const;											// draws the game score
		Gamefield();																// default constructor of game
	private:
		uint uiScore, uiLevel, uiLinesRemaining;							// the current score, level and the remaining lines till next level
		const uint uiMaxLines = 5;											// lines count for each level, reduce them to 0 to proceed to the next level
		const uint uiDeathRow = 4;											// postion of the Death Row, the game ends if a Tetromino block sticks out above this row
		bool isGameOver;														// shows if the game is finished
		bool isAutoPlay;															// switch for AI play - not implemented yet
		eFieldElement eNextTetromino;									// the next Tetromino that will be created after the current one. It is displayed in the nextTetromino window
		eFieldElement field[uiFieldHeight][uiFieldWidth];			// array with the game field
		Tetromino * t;															// the current Tetromino
		void PersistTetromino();												// manifests the Tetromino
		bool canRemoveLines();												// tries to remove lines
		bool isOverDeathRow();												// checks if Tetromino is over Death Row and terminates game
		eFieldElement CreateNextTetromino(eFieldElement eFE = eFieldElement::empty);	// determines the next Tetromino
		Tetromino *MakeTetromino(eFieldElement);					// creates the new Tetromino
};
