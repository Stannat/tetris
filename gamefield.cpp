#include <vector>
#include <ctime>
#include <cmath>
#include "gamefield.hpp"

#pragma region Initialization

Gamefield::Gamefield(){
	// create a new Tetromino and initialize game
	Gamefield::eNextTetromino = Gamefield::CreateNextTetromino();
	Gamefield::Init();
	// initialize random number generator
	std::srand(std::time(NULL));
}

// creates new Tetromino and computes next Tetromino type
eFieldElement Gamefield::CreateNextTetromino(eFieldElement eNextTetromino){
	assert (eNextTetromino != eFieldElement::wall);
	// if no Tetromino specified (empty) a random Tetromino is generated 
	if(eNextTetromino == eFieldElement::empty)
		eNextTetromino = (eFieldElement)((std::rand() % (int)eFieldElement::count) + 2);
	assert (eNextTetromino != eFieldElement::empty && eNextTetromino != eFieldElement::wall);
	
	// the Tetromino spwans at top in the center of the gamefield
	switch(eNextTetromino){
		case eFieldElement::T:
		Gamefield::t = new T_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::L:
		Gamefield::t = new L_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::L2:
		Gamefield::t = new L2_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::Z:
		Gamefield::t = new Z_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::S:
		Gamefield::t = new S_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::I:
		Gamefield::t = new I_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		case eFieldElement::O:
		Gamefield::t = new O_Tetromino((uint)0, (uint)(uiFieldWidth/2)-1);
		break;
		default:
		break;
	}
	// compute next Tetromino
	return (eFieldElement)((rand() % (int)eFieldElement::count) + 2);
}

// creates a Tetromino based on argument
Tetromino *Gamefield::MakeTetromino(eFieldElement eNextTetromino){
	assert (eNextTetromino != eFieldElement::empty && eNextTetromino != eFieldElement::wall);
	
	switch(eNextTetromino){
		case eFieldElement::T:
		return new T_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::L:
		return new L_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::L2:
		return new L2_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::Z:
		return new Z_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::S:
		return new S_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::I:
		return new I_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		case eFieldElement::O:
		return new O_Tetromino((uint)0, (uint)(uiFieldWidth/2));
		break;
		default:
		break;
	}
	
	// this case should not occur 
	// but if it occurs, it will return an I-Tetromino by default
	return new I_Tetromino((uint)0, (uint)(uiFieldWidth/2));
}

// initialize relevant game mechanic stuff
void Gamefield::Init(){
	// left and right field borders
	for(uint uiRow = 0; uiRow < Gamefield::uiFieldHeight - 1; uiRow++){
		for(uint uiCol = 1; uiCol < Gamefield::uiFieldWidth - 1; uiCol++)
			Gamefield::field[uiRow][uiCol] = eFieldElement::empty;
		
		Gamefield::field[uiRow][0] = eFieldElement::wall;
		Gamefield::field[uiRow][Gamefield::uiFieldWidth - 1] =  eFieldElement::wall;
	}
	// bottom border
	for(uint uiCol = 0; uiCol < Gamefield::uiFieldWidth; uiCol++)
		Gamefield::field[Gamefield::uiFieldHeight - 1][uiCol] = eFieldElement::wall;
	
	// game state
	Gamefield::eState = eGameState::eRunning;
	Gamefield::isGameOver = false;
	// game stats
	Gamefield::uiScore = 0;
	Gamefield::uiLevel = 0;
	// start with one field per secod, decrease counter to increase game speed
	Gamefield::uiMaxCounter = 100;
	Gamefield::uiLinesRemaining = Gamefield::uiMaxLines;
}

#pragma endregion Initialization

#pragma region Graphics
// initialize windows
void Gamefield::InitWins(){
	uint uiBoardWidth = 2 * Gamefield::uiFieldWidth;
	// the number of rows in the board window should be uiFieldHeight + 1 to fit to the logical gamefield
	// window width of board has to be twice the defined size because visually appealing squares are required
	Gamefield::board = newwin(Gamefield::uiFieldHeight + 1, uiBoardWidth, 0, 0);
	
	// start with an offset with respect to width of board (uiBoardSize + 2)
	// "2" is the distance between the board window and the other windows
	Gamefield::nextTetromino = newwin(6, 12, 0, uiBoardWidth + 2);
	Gamefield::info = newwin(4, 12, 6, uiBoardWidth + 2);
	Gamefield::score = newwin(8, 12, 10, uiBoardWidth + 2);
}

// bind colors to numbers.
// the numbers will be used for drawing Tetrominos on the board
void Gamefield::InitColors(){
	start_color();
	init_pair((short int)eFieldElement::T, COLOR_MAGENTA, COLOR_BLACK);
	init_pair((short int)eFieldElement::L, COLOR_BLUE, COLOR_BLACK);
	init_pair((short int)eFieldElement::L2, COLOR_BLUE, COLOR_BLACK);
	init_pair((short int)eFieldElement::Z, COLOR_RED, COLOR_BLACK);
	init_pair((short int)eFieldElement::S, COLOR_GREEN, COLOR_BLACK);
	init_pair((short int)eFieldElement::I, COLOR_CYAN, COLOR_BLACK);
	init_pair((short int)eFieldElement::O, COLOR_YELLOW, COLOR_BLACK);
}

// displays board with Tetrominos
void Gamefield::Display_board() const
{
	WINDOW* w = Gamefield::board;
	// get field with current Tetromino
	std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  field = 
	Gamefield::GetFieldWithMovingTetromino();
	// draw box around the window
	box(w, 0, 0);

	for (uint uiRow = 0; uiRow < Gamefield::uiFieldHeight -1; uiRow++) {
		// move cursor inside window
		// start drawing from uiRow+1 to prevent the first row of overlapping the upper game board edge
		// a single Tetris cell has the width of 2 units, so in order to move, start at pos. "2"
		wmove(w, uiRow+1, 2);
		// uiCol=0 and uiCol=11 are the left and right border of the gamefield
		// these borders are not to be drawn
		for (uint uiCol = 1; uiCol < Gamefield::uiFieldWidth -1; uiCol++) {
			// fill with empty space if there is no Tetromino block at the given position
			if(field[uiRow][uiCol] == eFieldElement::empty){
				// display the death row;
				// draw the death row only if it should be used at all (uiDeathRow > 0)
				if (Gamefield::uiDeathRow > 0 && uiRow == Gamefield::uiDeathRow){
					waddch(w, '_');
					waddch(w, '_');
				}
				else{ 
					waddch(w, ' ');
					waddch(w, ' ');
				}
			}
			else{// fill with respective Tetromino blocks at the given position
				// display of the death row
				if (Gamefield::uiDeathRow > 0 && uiRow == Gamefield::uiDeathRow){
					waddch(w, '_'|A_REVERSE|COLOR_PAIR((unsigned int) field[uiRow][uiCol]));
					waddch(w, '_'|A_REVERSE|COLOR_PAIR((unsigned int) field[uiRow][uiCol]));
				}
				else{
					waddch(w, ' '|A_REVERSE|COLOR_PAIR((unsigned int) field[uiRow][uiCol]));
					waddch(w, ' '|A_REVERSE|COLOR_PAIR((unsigned int) field[uiRow][uiCol]));
				}
			}
		}
	}
	// copy the board window to virtual screen
	wnoutrefresh(w);
}

// displays the next Tetromino
void Gamefield::Display_nextTetromino()
{
	// prepare Tetromino related stuff
	WINDOW* w = Gamefield::nextTetromino;
	assert(w != NULL);
	
	Tetromino * t_next = Gamefield::MakeTetromino(Gamefield::eNextTetromino);
	assert(t_next != NULL);
	
	std::vector<eFieldElement> tetromino = t_next->GetShape();
	assert(&tetromino != NULL);
	
	eFieldElement eFormType = t_next->GetFormType();
	assert (eNextTetromino != eFieldElement::empty && eNextTetromino != eFieldElement::wall);
	
	uint uiTetSize = t_next->GetSize();
	assert(uiTetSize > 1);
	// draw the window and the next Tetromino
	wclear(w);
	box(w, 0, 0);

	for (uint uiRow = 0; uiRow < uiTetSize; uiRow++) {
		// move cursor to the position where y and x coordinates of the Tetromino are drawn
		wmove(w, uiRow +  t_next->GetY_preview(), t_next->GetX_preview());
			
		for (uint uiCol =  0; uiCol < uiTetSize; uiCol++) {
			if(tetromino[uiRow * uiTetSize + uiCol] == eFieldElement::empty){
				waddch(w, ' ');
				waddch(w, ' ');
			}
			else{
				waddch(w, ' '|A_REVERSE|COLOR_PAIR((unsigned int) eFormType));
				waddch(w, ' '|A_REVERSE|COLOR_PAIR((unsigned int) eFormType));
			}
		}
	}
	// destructors have to be implemented first for the inherited types before "delete" can be used
	// delete t_next;
	wnoutrefresh(w);
}

// Status window
void Gamefield::Display_info(){
	WINDOW* w = Gamefield::info;
	wclear(w);
	box(w, 0, 0);

	// show if the game is running, paused or finished
	mvwprintw(w, 1, 1, "Game state");
	switch(Gamefield::eState){
		case eGameState::eRunning:
		mvwprintw(w, 2, 2, "running");
		break;
		case eGameState::ePaused:
		mvwprintw(w, 2, 2, "paused");
		break;
		case eGameState::eGameOver:
		mvwprintw(w, 2, 2, "game over");
		break;
		default:
		break;
	}
	wnoutrefresh(w);
}

// window for game score
void Gamefield::Display_score() const{
	WINDOW* w = Gamefield::score;
	wclear(w);
	box(w, 0, 0);
	mvwprintw(w, 1, 1, "Score");
	mvwprintw(w, 2, 2, "%d", uiScore);
	mvwprintw(w, 3, 1, "Level");
	mvwprintw(w, 4, 2, "%d", uiLevel);
	mvwprintw(w, 5, 1, "Lines");
	mvwprintw(w, 6, 2, "%d", uiLinesRemaining);
	wnoutrefresh(w);
}

#pragma endregion Graphics


#pragma region GameInformation

bool Gamefield::IsGameOver() const{
	return Gamefield::isGameOver;
}

// Getter for type of Tetromino form
eFieldElement Gamefield::GetFormType(){
	return Gamefield::t->GetFormType();
}

// Getter for position of Tetromino
std::pair<uint, uint> Gamefield::GetTetrominoPos() const{
	return std::make_pair(Gamefield::t->y, Gamefield::t->x);
}

// get the field without the current Tetromino
std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  Gamefield::GetField() const {
	std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  field;
	for(uint uiRow=0; uiRow < Gamefield::uiFieldHeight; uiRow++){
		for(uint uiCol=0; uiCol < Gamefield::uiFieldWidth; uiCol++){
			field[uiRow][uiCol] = Gamefield::field[uiRow][uiCol];
		}
	}
	return field;
}

// get the field with the current Tetromino
std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  Gamefield::GetFieldWithMovingTetromino() const {
	std::array<std::array<eFieldElement, Gamefield::uiFieldWidth>, Gamefield::uiFieldHeight>  fullField;
	std::vector<eFieldElement> tetromino = Gamefield::t->GetShape();
	
	for(uint uiRow=0; uiRow < Gamefield::uiFieldHeight; uiRow++){
		for(uint uiCol=0; uiCol < Gamefield::uiFieldWidth; uiCol++){
			fullField[uiRow][uiCol] = Gamefield::field[uiRow][uiCol];
		}
	}
	
	uint uiTetSize = Gamefield::t->GetSize();
	// just iterate over the Tetromino array and add the offset of its blocks position
	for(uint uiRow=0; uiRow < uiTetSize; uiRow++){
		for(uint uiCol=0; uiCol < uiTetSize; uiCol++){
			// only the positive values within game field scope should be checked
			// otherwise out of bounds access will occur
			bool isRowIndPos = uiRow + Gamefield::t->y >= 0;
			bool isColIndPos = uiCol + Gamefield::t->x >= 1;
			bool isColIndValid = uiRow + Gamefield::t->y < Gamefield::uiFieldHeight - 1;
			bool isRowIndValid = uiCol + Gamefield::t->x < Gamefield::uiFieldWidth - 1;
			
			
			if (isRowIndPos && isColIndPos && isColIndValid && isRowIndValid){
				assert(uiRow + Gamefield::t->y >= 0);
				// block is not allowed to stick in the wall
				assert(uiRow + Gamefield::t->y < Gamefield::uiFieldHeight - 1);
				assert(uiCol + Gamefield::t->x >= 1);
				assert(uiCol + Gamefield::t->x < Gamefield::uiFieldWidth - 1);
				
				bool isFieldPosEmpty = fullField[uiRow + Gamefield::t->y][uiCol + Gamefield::t->x] == eFieldElement::empty;
				if(isFieldPosEmpty)
					// set offset for y and x positions in the game field
					fullField[uiRow + Gamefield::t->y][uiCol + Gamefield::t->x] = (eFieldElement) tetromino[uiRow * uiTetSize + uiCol];
			}
		}
	}
	
	return fullField;
}

#pragma endregion GameInformation


#pragma region FieldManipulation

// the classic update loop
// this method is responsible for the Tetromino movement downwards
void Gamefield::Update() {
	
	if(Gamefield::eState == eGameState::eRunning)
		Gamefield::MoveTetromino();
}

// moves Tetromino down one (as default) block and 
// creats a new if it hits something
bool Gamefield::MoveTetromino(uint uiTimes) {
	// perform only if game is running
	if(Gamefield::eState != eGameState::eRunning)
		return false;
	// remember old Tetromino position before setting the new one
	uint uiOldY = Gamefield::t->y;
	Gamefield::t->y += uiTimes;
	// collision handling
	if(Gamefield::IsColliding()){
		Gamefield::t->y = uiOldY;
		Gamefield::t->isAlive = false;
		// manifest Tetromino in game field and kill filled lines
		Gamefield::PersistTetromino();
		Gamefield::canRemoveLines();
		// delete old Tetromino and create a new one
		// destructors have to be implemented first for the inherited types before "delete" can be used
		// delete Gamefield::t;
		Gamefield::eNextTetromino = Gamefield::CreateNextTetromino(Gamefield::eNextTetromino);
		// check for game over
		if (isOverDeathRow()){
			Gamefield::eState = eGameState::eGameOver;
			Gamefield::isGameOver = true;
		}
		return false;
	}
	return true;
}

// checks if Tetromino sticks out above the "game over" - Death Row
bool Gamefield::isOverDeathRow() {
	for(uint uiRow=0; uiRow <= Gamefield::uiDeathRow; uiRow++){
		for(uint uiCol=1; uiCol < Gamefield::uiFieldWidth - 1; uiCol++){
			// bounds check
			assert(uiRow >= 0);
			assert(uiRow < Gamefield::uiFieldHeight);
			assert(uiCol >= 0);
			assert(uiCol < Gamefield::uiFieldWidth);
			
			// check if a row has at least one Tetromino block in it
			if (Gamefield::field[uiRow][uiCol] != eFieldElement::empty)
				return true;
		}
	}
	return false;
}

// manifests a Tetromino
void Gamefield::PersistTetromino() {
	std::vector<eFieldElement> tetromino = Gamefield::t->GetShape();
	uint uiTetSize = Gamefield::t->GetSize();
	
	// only the iteration over the Tetromino is neccessary
	// the Tetromino offset is added to the iteration index in order to determine the y and x coordinates
	for(uint uiRow=0; uiRow < uiTetSize; uiRow++){
		for(uint uiCol=0; uiCol < uiTetSize; uiCol++){
			assert(uiRow * uiTetSize + uiCol >= 0);
			assert(uiRow * uiTetSize + uiCol < uiTetSize * uiTetSize);

			// a check for a positive position index is not necessary because every visible block
			// in a Tetromino is assured to have already positive index array values
			if (tetromino[uiRow * uiTetSize + uiCol] != eFieldElement::empty){
				assert(uiRow + Gamefield::t->y >= 0);
				assert(uiRow + Gamefield::t->y < Gamefield::uiFieldHeight);
				assert(uiCol + Gamefield::t->x >= 0);
				assert(uiCol + Gamefield::t->x < Gamefield::uiFieldWidth);
				Gamefield::field[uiRow + Gamefield::t->y][uiCol + Gamefield::t->x] = tetromino[uiRow * uiTetSize + uiCol];
			}
		}
	}
}

// kills full lines and increases game score and level
// also the Linecount is changed
bool Gamefield::canRemoveLines() {
	uint uiCombo = 0;
	for(uint uiRow=0; uiRow < Gamefield::uiFieldHeight-1; uiRow++){
		bool isFullRow = true;
		for(uint uiCol=1; uiCol < Gamefield::uiFieldWidth-1; uiCol++){
			// an empty field means that the current row will not contain a full line,
			// the next columns are skipped and the next row is evaluated
			if (Gamefield::field[uiRow][uiCol] == eFieldElement::empty){
				isFullRow = false;
				uiCol = Gamefield::uiFieldWidth-1;
			}
		}

		if(isFullRow){
			// move all rows above the current row one row down
			for(uint uiRow2=uiRow-1; uiRow2 > 0; uiRow2--){
				for(uint uiCol2=1; uiCol2 < Gamefield::uiFieldWidth - 1; uiCol2++)
					Gamefield::field[uiRow2+1][uiCol2] = Gamefield::field[uiRow2][uiCol2];
			}
			uiCombo++;
		}
	}
	
	// adapt score, level, remaining lines and game speed
	Gamefield::uiScore += pow(10, uiCombo);
	// remove just as much at most as is required to get to the next level
	Gamefield::uiLinesRemaining -= std::min(uiCombo, Gamefield::uiLinesRemaining);
	if (Gamefield::uiLinesRemaining == 0){
		Gamefield::uiLinesRemaining = Gamefield::uiMaxLines;
		Gamefield::uiLevel++;
		// increase game speed
		Gamefield::uiMaxCounter *= 0.8;
	}
	
	return uiCombo > 0;
}

//  tries to move the Tetromino to right or left
bool Gamefield::TryMoveSideways(bool right) const {
	// only if game is active
	if(!Gamefield::t->isAlive || Gamefield::eState != eGameState::eRunning)
		return false;
	// remember the old x position
	int iOldX = Gamefield::t->x;
	if (right)
		Gamefield::t->x++;
	else{
		Gamefield::t->x--;
	}
	// reset Tetromino position
	if(Gamefield::IsColliding()){
		Gamefield::t->x = iOldX;
		
		assert(Gamefield::t->x == iOldX);
		return false;
	}
	
	assert(Gamefield::t->x != iOldX);
	return true;
}

// collision check of Tetromino with environment
bool Gamefield::IsColliding() const {
	// get the Tetromino array or vecor
	std::vector<eFieldElement> tetromino = Gamefield::t->GetShape();
	uint uiTetSize = Gamefield::t->GetSize();
	for(uint uiRow=0; uiRow < uiTetSize; uiRow++){
		for(uint uiCol=0; uiCol < uiTetSize; uiCol++){
			// boundary checks
			assert(uiRow * uiTetSize + uiCol >= 0);
			assert(uiRow * uiTetSize + uiCol < uiTetSize * uiTetSize);
			// only filled blocks within the Tetromino are checked
			bool isTetrEmpty = tetromino[uiRow * uiTetSize + uiCol] == eFieldElement::empty;
			
			if(!isTetrEmpty){
				// boundary checks
				bool isYinLowerBounds  = uiRow + Gamefield::t->y >= 0;
				bool isYinUpperBounds = uiRow + Gamefield::t->y < Gamefield::uiFieldHeight - 1;
				
				bool isXinLowerBounds = uiCol + Gamefield::t->x > 0;
				bool isXinUpperBounds = uiCol + Gamefield::t->x < Gamefield::uiFieldWidth - 1;
				
				if(isYinLowerBounds && isYinUpperBounds && isXinLowerBounds && isXinUpperBounds){
					assert(uiRow + Gamefield::t->y >= 0);
					assert(uiCol + Gamefield::t->x >= 0);
					assert(uiRow + Gamefield::t->y < Gamefield::uiFieldHeight);
					assert(uiCol + Gamefield::t->x < Gamefield::uiFieldWidth);
					// check if position on gamefield is not empty, e.g. gamefield wall or another Tetromino
					if (Gamefield::field[uiRow + Gamefield::t->y][uiCol + Gamefield::t->x] != eFieldElement::empty)
						return true;
				}
				else // if Tetromino is not within defined bounds then it is over the gamefield border
					return true;
			}
		}
	}
	return false;
}

// rotates the Tetromino at least one time
// returns True if rotation was successful
bool Gamefield::RotateTetromino(uint uiTimes) const {
	bool rotationStatus = false;
	bool isTAlive = Gamefield::t->isAlive;
	bool isStateRunning = Gamefield::eState == eGameState::eRunning;
	if(isTAlive && isStateRunning){
		Gamefield::t->RotateCW(uiTimes);
		if(Gamefield::IsColliding()){
			Gamefield::t->ResetRotation();
			rotationStatus = false;
		}
		else
			rotationStatus = true;
	}
	else
		rotationStatus = false;
	
	return rotationStatus;
}

#pragma endregion FieldManipulation
