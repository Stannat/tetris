#pragma once
#include <sys/types.h>
#include <vector>
#include <curses.h>
#include <cassert>

// specifies the element on field
enum class eFieldElement : signed int { empty, wall, T, L, L2, Z, S, I, O, count = O - T + 1};

class Tetromino{
	public:
		int x, y, uiRot;																// position values of the whole Tetromino
		bool isAlive;																	// living status, not used yet
		uint GetRot() const;														// rotation state of the Tetromino
		void RotateCW(uint uiRot = 1);											// rotates clockwise
		void RotateCCW(uint uiRot = 1);										// rotates counter clockwise
		void ResetRotation();														// resets the last roatation
		virtual std::vector<eFieldElement> GetShape() const = 0;	// container for the Tetromino shape
		virtual uint GetSize() const = 0;										// size of the Tetromino container
		virtual uint GetX_preview() const = 0;								// x position in the Tetromino preview window
		virtual uint GetY_preview() const = 0;								// y position in the Tetromino preview window
		virtual eFieldElement GetFormType() const = 0;					// enum type of the Tetromino
		Tetromino();
		Tetromino(uint, uint);
	private:
		virtual void Init() = 0;
	protected:
		std::vector<eFieldElement> shapeOld;								// backup for the old Tetromino contaier
		std::vector<eFieldElement> shapeNew;							// the current Tetromino container
};

class Two_Tetromino:public Tetromino{
	public:
		static uint const uiSize = 2;
		uint GetSize() const;	
		Two_Tetromino(uint, uint);
};

class Three_Tetromino:public Tetromino{
	public:
		static uint const uiSize = 3;
		uint GetSize() const;
		Three_Tetromino(uint, uint);
};

class Four_Tetromino:public Tetromino{
	public:
		static uint const uiSize = 4;
		uint GetSize() const;
		Four_Tetromino(uint, uint);
};

class O_Tetromino:public Two_Tetromino{
	public:
		static const uint x_preview = 4,  y_preview = 2;
		static const eFieldElement f = eFieldElement::O;	
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		O_Tetromino(uint, uint);
	private:
		void Init();
};

class T_Tetromino:public Three_Tetromino{
	public:
		static const uint x_preview = 3,  y_preview = 1;
		static const eFieldElement f = eFieldElement::T;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		T_Tetromino(uint, uint);
	private:
		void Init();
};

class L_Tetromino:public Three_Tetromino{
	public:
		static const uint x_preview = 2,  y_preview = 1;
		static const eFieldElement f = eFieldElement::L;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		L_Tetromino(uint, uint);
	private:
		void Init();
};

class L2_Tetromino:public Three_Tetromino{
	public:
		static const uint x_preview = 2,  y_preview = 1;
		static const eFieldElement f = eFieldElement::L2;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		L2_Tetromino(uint, uint);
	private:
		void Init();
};

class Z_Tetromino:public Three_Tetromino{
	public:
		static const uint x_preview = 3,  y_preview = 2;
		static const eFieldElement f = eFieldElement::Z;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		Z_Tetromino(uint, uint);
	private:
		void Init();
};

class S_Tetromino:public Three_Tetromino{
	public:
		static const uint x_preview = 3,  y_preview = 2;
		static const eFieldElement f = eFieldElement::S;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		S_Tetromino(uint, uint);
	private:
		void Init();
};

class I_Tetromino:public Four_Tetromino{
	public:
		static const uint x_preview = 2,  y_preview = 1;
		static const eFieldElement f = eFieldElement::I;
		uint GetX_preview() const;
		uint GetY_preview() const;
		std::vector<eFieldElement> GetShape() const;
		eFieldElement GetFormType() const;
		I_Tetromino(uint, uint);
	private:
		void Init();
};